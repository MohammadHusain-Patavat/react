import React, { useState } from "react";
import Header from "./components/Header";
import Form from "./components/Form";
import Table from "./components/Table";
export default function App() {
  const [data, setdata] = useState([
    {
      first_name: "Mohammad husain",
      last_name: "patavat",
      email: "mohammadhusian@gmail.com",
    },
    {
      first_name: "Raheman ali",
      last_name: "Agariya",
      email: "rahemanali@gmail.com",
    },
    {
      first_name: "Vajir ali",
      last_name: "Asamadi",
      email: "vajirali@gmail.com",
    },
  ]);

  return (
    <>
      <Header />
      <Form data={data} setdata={setdata} />
      <Table data={data} setdata={setdata} />
    </>
  );
}
