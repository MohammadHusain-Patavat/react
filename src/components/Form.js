import React, { useState } from "react";

export default function Form({ data, setdata }) {
  const [formdata, setFormdata] = useState({});
  const [formerror, setformerror] = useState({});

  const formSubmitHandler = (e) => {
    e.preventDefault();
    if (
      formdata.last_name != null &&
      formdata.first_name != null &&
      formdata.email != null
    ) {
      setdata((prev) => {
        return [...prev, formdata];
      });
    } else {
      var inputs = document.getElementsByClassName("form-field");
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value === null || inputs[i].value === "") {
          let key = inputs[i].name + "_error";
          setformerror((prev) => {
            return { ...prev, [key]: "Field must required" };
          });
        }
      }
    }
  };

  const formChangeHandler = (e) => {
    setFormdata((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  const formvalidate = (e) => {
    if (e.target.value === "" || e.target.value === null) {
      let key = e.target.name + "_error";
      setformerror((prev) => {
        return { ...prev, [key]: "Field required" };
      });
      return;
    } else {
      let key = e.target.name + "_error";
      setformerror((prev) => {
        return { ...prev, [key]: "" };
      });
    }

    if (e.target.name == "email") {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(e.target.value)) {
        setformerror((prev) => {
          return { ...prev, email_error: "" };
        });
      } else {
        setformerror((prev) => {
          return { ...prev, email_error: "Invalid Email" };
        });
      }
    }

    if (e.target.name == "last_name" || e.target.name == "first_name") {
      let key = e.target.name + "_error";
      if (e.target.value.length < 3) {
        setformerror((prev) => {
          return { ...prev, [key]: "Must contain atleast 3 character" };
        });
      } else {
        setformerror((prev) => {
          return { ...prev, [key]: "" };
        });
      }
    }
  };

  return (
    <div className="container my-4 mt-5">
      <h3>React Form</h3>
      <form onSubmit={formSubmitHandler}>
        <div className="form-group">
          <label htmlFor="first_name">First Name</label>
          <input
            type="text"
            className="form-control form-field"
            name="first_name"
            id="first_name"
            placeholder="Enter first name"
            onChange={formChangeHandler}
            onBlur={formvalidate}
          />
          <small className="text-danger">{formerror.first_name_error}</small>
        </div>

        <div className="form-group">
          <label htmlFor="last_name">Last Name</label>
          <input
            type="text"
            name="last_name"
            id="last_name"
            className="form-control form-field"
            placeholder="Last Name"
            onChange={formChangeHandler}
            onBlur={formvalidate}
          />
          <small className="text-danger">{formerror.last_name_error}</small>
        </div>

        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="text"
            name="email"
            id="email"
            className="form-control form-field"
            placeholder="Email"
            onChange={formChangeHandler}
            onBlur={formvalidate}
          />
          <small className="text-danger">{formerror.email_error}</small>
        </div>

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}
