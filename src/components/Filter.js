import React from "react";

export default function Filter({ filter, setGlobalFilter }) {
  return (
    <div className="row">
      <div className="col-md-6">
        <div className="form-group">
          <label htmlFor="search">Search</label>
          <input
            className="form-control"
            type="text"
            value={filter || ""}
            placeholder="search..."
            id="search"
            onChange={(e) => setGlobalFilter(e.target.value)}
          />
        </div>
      </div>
    </div>
  );
}
